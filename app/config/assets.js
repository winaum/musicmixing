Assets = {
	"melody-select": {
		audio: {
			dir: '/audio/',
			items: [
				{id: 'icon-guitar', file: 'melody/Melody_Guitar'},
				{id: 'icon-guitar-acoustic', file: 'melody/Melody_Ukelele'},
				{id: 'icon-sax', file: 'melody/Melody_Saxophone'},
				{id: 'icon-trumpet', file: 'melody/Melody_Trumpet'},
				{id: 'icon-recorder', file: 'melody/Melody_Flute'},
			]
		}
	},
	"beats-select": {
		audio: {
			dir: '/audio/',
			items: [
				{id: 'icon-bong', file: 'beats/Percussion_Bongo'},
				{id: 'icon-drum-head', file: 'beats/Percussion_Shaker'},
				{id: 'icon-percussion', file: 'beats/Percussion_Tambourine'},
				{id: 'icon-snare', file: 'beats/Percussion_Snare'},
				{id: 'icon-shaker', file: 'beats/Percussion_Guiro'},
			]					
		}
	},
	"effects-select": {
		audio: {
			dir: '/audio/',
			items: [
				{id: 'icon-cymbals', file: 'effects/Effect_Finger_Cymbal'},
				{id: 'icon-maraca', file: 'effects/Effect_Egg_Shaker'},
				{id: 'icon-triangle', file: 'effects/Effect_Triangle'},
				{id: 'icon-bell', file: 'effects/Effect_Cowbell'},
				{id: 'icon-cymbals-blue', file: 'effects/Effect_Big_Cymbal'}
			]	
						
		}
	},
	"lyrics-select": {
		audio: {
			dir: '/audio/',			
			items: [
				{id: '1_1', file: 'lyrics/1_1'},
				{id: '1_2', file: 'lyrics/1_2'},
				{id: '1_3', file: 'lyrics/1_3'},
				{id: '1_4', file: 'lyrics/1_4'},
				{id: '1_5', file: 'lyrics/1_5'},
				{id: '1_6', file: 'lyrics/1_6'},
				{id: '1_7', file: 'lyrics/1_7'},
				{id: '1_8', file: 'lyrics/1_8'},
				{id: '1_9', file: 'lyrics/1_9'},
				{id: '1_10', file: 'lyrics/1_10'},
				{id: '1_11', file: 'lyrics/1_11'},
				{id: '1_12', file: 'lyrics/1_12'},
				{id: '1_13', file: 'lyrics/1_13'},
				{id: '1_14', file: 'lyrics/1_14'},
				{id: '1_15', file: 'lyrics/1_15'},
				
				{id: '2_1', file: 'lyrics/2_1'},				
				{id: '2_2', file: 'lyrics/2_2'},				
				{id: '2_3', file: 'lyrics/2_3'},				
				{id: '2_4', file: 'lyrics/2_4'},				
				{id: '2_5', file: 'lyrics/2_5'},				
				{id: '2_6', file: 'lyrics/2_6'},				
				{id: '2_7', file: 'lyrics/2_7'},				
				{id: '2_8', file: 'lyrics/2_8'},				
				{id: '2_9', file: 'lyrics/2_9'},				
				{id: '2_10', file: 'lyrics/2_10'},				
				{id: '2_11', file: 'lyrics/2_11'},				
				{id: '2_12', file: 'lyrics/2_12'},				
				{id: '2_13', file: 'lyrics/2_13'},				
				{id: '2_14', file: 'lyrics/2_14'},				
				{id: '2_15', file: 'lyrics/2_15'},				

				{id: '3_1', file: 'lyrics/3_1'},				
				{id: '3_2', file: 'lyrics/3_2'},				
				{id: '3_3', file: 'lyrics/3_3'},				
				{id: '3_4', file: 'lyrics/3_4'},				
				{id: '3_5', file: 'lyrics/3_5'},				
				{id: '3_6', file: 'lyrics/3_6'},				
				{id: '3_7', file: 'lyrics/3_7'},				
				{id: '3_8', file: 'lyrics/3_8'},				
				{id: '3_9', file: 'lyrics/3_9'},				
				{id: '3_10', file: 'lyrics/3_10'},				
				{id: '3_11', file: 'lyrics/3_11'},				
				{id: '3_12', file: 'lyrics/3_12'},				
				{id: '3_13', file: 'lyrics/3_13'},				
				{id: '3_14', file: 'lyrics/3_14'},				
				{id: '3_15', file: 'lyrics/3_15'},				

				{id: '4_1', file: 'lyrics/4_1'},				
				{id: '4_2', file: 'lyrics/4_2'},				
				{id: '4_3', file: 'lyrics/4_3'},				
				{id: '4_4', file: 'lyrics/4_4'},				
				{id: '4_5', file: 'lyrics/4_5'},				
				{id: '4_6', file: 'lyrics/4_6'},				
				{id: '4_7', file: 'lyrics/4_7'},				
				{id: '4_8', file: 'lyrics/4_8'},				
				{id: '4_9', file: 'lyrics/4_9'},				
				{id: '4_10', file: 'lyrics/4_10'},				
				{id: '4_11', file: 'lyrics/4_11'},				
				{id: '4_12', file: 'lyrics/4_12'},				
				{id: '4_13', file: 'lyrics/4_13'},				
				{id: '4_14', file: 'lyrics/4_14'},				
				{id: '4_15', file: 'lyrics/4_15'},							
			]
		}
	}
}