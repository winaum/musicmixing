'use strict';

angular
.module('musicmixingApp')
.config(['$routeProvider',function ($routeProvider) {
	$routeProvider
	.when('/', {
		templateUrl: 'scripts/modules/home/main.html',
		controller: 'MainController'
	})
	.when('/id/:id', {
		templateUrl: 'scripts/modules/home/main.html',
		controller: 'MainController'
	})
	.when('/melody-select', {
		templateUrl: 'scripts/modules/music/melody.html',
		controller: 'MelodyController',
		resolve:{
			reloadAssets: function(Audioplayer){		
				return Audioplayer.preloadResources('melody-select');
			}
		}		
	})
	.when('/beats-select', {
		templateUrl: 'scripts/modules/music/beats.html',
		controller: 'BeatsController',
		resolve:{
			reloadAssets: function(Audioplayer){	
				return Audioplayer.preloadResources('beats-select');
			}
		}				
	})
	.when('/effects-select', {
		templateUrl: 'scripts/modules/music/effects.html',
		controller: 'EffectsController',
		resolve:{
			reloadAssets: function(Audioplayer){	
				return Audioplayer.preloadResources('effects-select');
			}
		}				
	})
	.when('/lyrics-select', {
		templateUrl: 'scripts/modules/music/lyrics.html',
		controller: 'LyricsController',
		resolve: {
			reloadAssets: function(Audioplayer) {
				return Audioplayer.preloadResources('lyrics-select');
			}
		}
	})
	.when('/enter-name', {
		templateUrl: 'scripts/modules/site/enterName.html',
		controller: 'NameController',
	})
	.when('/submit-record', {
		templateUrl: 'scripts/modules/site/submitRecord.html',
		controller: 'RecordController',
	})
	.when('/thank-you', {
		templateUrl: 'scripts/modules/site/thankYou.html',
		controller: 'ThankYouController',
	})
	.otherwise({
		redirectTo: '/'
	});
}]);