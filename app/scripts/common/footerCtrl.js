'use strict';

angular.module('musicmixingApp')
    .controller('FooterCtrl', [
        '$scope', '$location',
        function ($scope, $location) {
            $scope.isHomePage = $location.path() === "/";
            $scope.isDoneRecording = ($location.path() === "/thank-you" || $location.path() === '/submit-record') ;

            $scope.$on('$routeChangeSuccess', function(){
                $scope.isHomePage = $location.path() === "/";
                $scope.step = $scope.getStepNumber($location.path());
                $scope.isMusicPage = ($scope.step > 0) ? true: false; 
                $scope.isDoneRecording = ($location.path() === "/thank-you" || $location.path() === '/submit-record') ;

                if ($location.path() === "/lyrics-select")
                    $('.header').addClass('hide-on-light');
                else 
                    $('.header').removeClass('hide-on-light');

            });

            $scope.getStepNumber = function(currentLocation){
                switch(currentLocation){
                    case '/melody-select': return 1; break;
                    case '/beats-select': return 2; break;
                    case '/effects-select': return 3; break;
                    case '/lyrics-select': return 4; break;
                    default: return 0;
                }
            };

            $scope.step = $scope.getStepNumber($location.path());
            $scope.isMusicPage = ($scope.step > 0) ? true: false; 
        }
    ]);
