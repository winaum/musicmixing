'use strict';
angular.module('musicmixingApp')
	.factory('AudioMixer', ['$cookies','$rootScope','Audioplayer',
		function($cookies, $rootScope, Audioplayer){
			return {
				musics: {
					items: [],
					prev: null,
					isPlaying: false,
					isTrackStarted: false,
					getById: function(index) {
						return this.items[index];
					},
					addMelody: function(music) {
						this.items[0] = music;
					},
					addBeats: function(music) {
						this.items[1] = music;
					},
					addEffects: function(music) {
						this.items[2] = music;
					},
					addSweet: function(music) {
						this.items[3] = music;
					},
					addNice: function(music) {
						this.items[4] = music;
					},
					addNaughty: function(music) {
						this.items[5] = music;
					},
					addForgiving: function(music) {
						this.items[6] = music;
					},
					playTimeLine: function() 
					{
						var self = this;

						if (self.isTrackStarted) {
							//return false;
						}

						self.isTrackStarted = true;
						Audioplayer.stopAll();

						for (var i in  self.items) {
							Audioplayer.play(self.items[i]);
						}	

						//self.loopTrack();

					},
					loopTrack : function() {
						var self = this;

						var looping = setInterval(function(){
							console.log(self.isTrackStarted);
							
							if (self.isTrackStarted) {							
								for (var i in  self.items) {
									Audioplayer.play(self.items[i]);
								}	
							} else {
								self.isTrackStarted = true;
								Audioplayer.stopAll();
								clearInterval(looping);
							}


						},20000);	
					},
					haltTrack: function() {
						Audioplayer.stopAll();
						this.isTrackStarted = false;
					},
					generateDefaultLyrics: function() {
						var self = this;
						self.items[3] = (typeof self.items[3] !== 'undefined' ) ? self.items[3] : '1_1';
						self.items[4] = (typeof self.items[4] !== 'undefined' ) ? self.items[4] : '2_1';
						self.items[5] = (typeof self.items[5] !== 'undefined' ) ? self.items[5] : '3_1';
						self.items[6] = (typeof self.items[6] !== 'undefined' ) ? self.items[6] : '4_1';

					},
					getAll: function() {
						return this.items;
					},
					getLyric: function(type) {
						var music = '',
							self  = this;

						switch (type) {
							case 'Sweet':
								music = (self.items[3]) ? self.items[3]: '1_1';
								break;
							case 'Nice':
								music = (self.items[4]) ? self.items[4]: '2_1';
								break;
							case 'Naughty':
								music = (self.items[5]) ? self.items[5]: '3_1';
								break;
							case 'Forgiving':
								music = (self.items[6]) ? self.items[6]: '4_1';
								break;
						}

						var index  = parseInt(music.substring(2)) - 1;
						if (type && music)
							return Lyrics[type][index];

						return '';
					},
					countTrackList: function () {
						var self = this,
							total = 0;

						for (var i in self.items) {
							if (typeof self.items[i] != 'undefined') {
								total++;
							}
						}

						return total;
					},

					/**
					 * @todo refactor this later
					 */
					showInstrumentsAnimation: function($container) {
						var	$filler1   = $container.find('.filler-1'),
				    		$filler2   = $container.find('.filler-2'),
				    		$filler3   = $container.find('.filler-3'),
				    		$filler4   = $container.find('.filler-4'),
				    		$filler5   = $container.find('.filler-5');

				    	var melody = '' + this.getById(0),
				    		beats  = '' + this.getById(1),
				    		effects  = '' + this.getById(2);
						
						//$filler2.css('left','-90px');

						switch (melody) {
							case 'icon-sax':
								$filler1.fadeOut();
								$filler2.css('background-image','url(/images/icons/icon-sax.png)');
							//	$filler2.css('left','-172px');
								$filler2.fadeIn();
								break;
							case 'icon-guitar':
								$filler1.fadeOut();
								$filler2.css('background-image','url(/images/filler2.png)');
								$filler2.fadeIn();
								break;
							case 'icon-guitar-acoustic':
								$filler2.fadeOut();
								$filler1.css('background-image','url(/images/filler1.png)'); 
								$filler1.fadeIn();
								break;
							case 'icon-trumpet':
								$filler2.fadeOut();
								$filler1.css('background-image','url(/images/icons/icon-trumpet.png)'); 
								$filler1.fadeIn();
								break;
							case 'icon-recorder':
								$filler2.fadeOut();
								$filler1.css('background-image','url(/images/icons/icon-recorder.png)'); 
								$filler1.fadeIn();
								break;
						}

						switch (beats) {
							case 'icon-bong':
								$filler3.css('background-image','url(/images/filler3.png)'); 
								$filler4.css('background-image','url(/images/filler4.png)'); 
								$filler3.fadeIn();
								$filler4.fadeIn();
								break;
							case 'icon-drum-head':
								$filler3.css('background-image','url(/images/icons/icon-drum-head.png)'); 
								$filler4.css('background-image','url(/images/icons/icon-drum-head.png)'); 
								$filler3.fadeIn();
								$filler4.fadeIn();
								break;
							case 'icon-percussion':
								$filler3.css('background-image','url(/images/icons/icon-percussion.png)'); 
								$filler4.css('background-image','url(/images/icons/icon-percussion.png)'); 
								$filler3.fadeIn();
								$filler4.fadeIn();
								break;
							case 'icon-snare':
								$filler3.css('background-image','url(/images/icons/icon-snare.png)'); 
								$filler4.css('background-image','url(/images/icons/icon-snare.png)'); 
								$filler3.fadeIn();
								$filler4.fadeIn();
								break;
							case 'icon-shaker':
								$filler3.css('background-image','url(/images/icons/icon-shaker.png)'); 
								$filler4.css('background-image','url(/images/icons/icon-shaker.png)'); 
								$filler3.fadeIn();
								$filler4.fadeIn();
								break;
						}		

						switch (effects) {
							case 'icon-cymbals':
								$filler5.css('background-image','url(/images/icons/icon-cymbals.png)'); 
								$filler5.fadeIn();
								break;
							case 'icon-maraca':
								$filler5.css('background-image','url(/images/icons/icon-maraca.png)'); 
								$filler5.fadeIn();
								break;
							case 'icon-triangle':
								$filler5.css('background-image','url(/images/icons/icon-triangle.png)'); 
								$filler5.fadeIn();
								break;
							case 'icon-bell':
								$filler5.css('background-image','url(/images/icons/icon-bell.png)');
								$filler5.fadeIn(); 
								break;
							case 'icon-cymbals-blue':
								$filler5.css('background-image','url(/images/icons/icon-cymbals-blue.png)'); 
								$filler5.fadeIn();
								break;

						}

						


					},
				},
			}
}]);		