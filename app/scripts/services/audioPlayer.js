'use strict';

angular.module('musicmixingApp')
	.factory('Audioplayer', ['$q','$rootScope',function($q,$rootScope){
		return {
			currentlyPlaying: '',
			tracks: {},
			status: '',
			playlist: [],
			timeout: 30000, //10 seconds max for preloading....
			preloadResources: function(trackId){
				//var d = $q.defer();			
				
				var self = this;			
				var useStreaming = false;
				var audios = Assets[trackId].audio.items;

				// wait for sound manager 2 to load
				var waiting = setInterval(function() {
					if (soundManagerReady) {

						for (var i in audios) {					


							var track = audios[i].id, 
							file = audios[i].file,
							baseUrl = Assets[trackId].audio.dir;	

							if (self.tracks[track] == undefined) {
								self.tracks[track] = soundManager.createSound({
									  id: track,
									  url: baseUrl + file + '.mp3',
									  autoLoad: true,
									  autoPlay: false,
									  onfinish: function(){					  	
									  	self.setStatus('');
									  	this.unload();
									  },
									  onplay: function(){					  	
									  	self.setStatus('playing');
									  	self.currentlyPlaying = this;
									  }									  
								});	

							}
						}			
						//d.resolve();
						clearInterval(waiting);
									
					}
				}, 1000);			

				//return d.promise;
			},

			play: function(sound, callback){	

				var d = $q.defer();		
				
				if(sound instanceof Array){					
					var index = 0;	
					this.playlist = sound;				
					var playState = this._playOne(this.playlist[index++]);
					var self = this;

					playState.then(function(){
						if(index < self.playlist.length)
							playState = self._playOne(self.playlist[index++]);
						else{
							if(callback !== undefined){
								callback();
							}
							d.resolve();						
						}
					});
				}else{					
					this._playOne(sound).then(
						function(){
							if(callback !== undefined){
								callback();
							}

							d.resolve();						
						}
					);					
				}
				return d.promise;

			},
			setStatus: function(status){
				this.status = status;
			},

			isPlaying: function(){
				return this.status === 'playing';				
			},
			stopById : function(id) {
				soundManager.stop(id);
			},
			playById: function(id) {
				soundManager.play(id);
			},
			stopAll: function () {
				soundManager.stopAll();
			},
			stop: function(){
				this.playlist = [];
				this.currentlyPlaying.stop();			
				this.currentlyPlaying.unload();
			},

			_playOne: function(sound){				
				var d = $q.defer();				
				var self = this;

				if(this.tracks[sound] != undefined)
					this.tracks[sound].play();
				else
					self.status = '';

				var statusChecker = setInterval(
					function(){
						if(self.status === ''){
							d.resolve();
							clearInterval(statusChecker);
						}
					}, 1500);
				
				return d.promise;
			}
		};
}]);