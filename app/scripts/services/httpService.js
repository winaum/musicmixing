'use strict';

/**
 * @todo supply correct returns for each callback
 *  -refactor
 *  -cleanups
 */
angular.module('musicmixingApp')
	.factory('HttpService',  ['$http','$q','$rootScope', function($http, $q, $rootScope) {
   		return {
   			data: {
   				songFileName: '', //straight from the API
		        melody: '',
		        beats: '',
		        effects: '',
		        lyrics: [],
		        email: '',
		       	name: '',
		        state: "NSW",
		        answer: '',
		        mumName: '',
		        terms: false,
		        id : 0,
		        title: '',
		        callResult: null,
		        filename: '' //just the audio filename e.g  BDCBAAA.mp3
   			},
   			baseUrl: 'http://cyphavm.wearecypha.com.au',   			
   			getRandomMix: function(musicId) {
				
   				var self = this; 
   				var id  = (musicId) ? musicId : 0;

		        $.ajax({
		            url: self.baseUrl + '/service/api/mix/' + id,
		            type: 'GET',
		            dataType: 'json',
		            success: function (resp) {
		                self.data.songFileName = resp.result;
						$rootScope.$broadcast('updateSong', self.data.songFileName);

		            },
		            error: function (resp) {
		                console.log('error');
		                console.log(resp);
		            }
		        });   
   			},
   			sendEmail: function(params) {
   				var self = this; 

				var data = {
            		name: "",
            		email: "",
            		id: 0
        		};

   				if (params) {
   					data.name = (params.name) ? (params.name) : '';	
   					data.email = (params.email) ? (params.email) : '';	
   					data.id = (params.id) ? (params.id) : 0;	
   				}

   				if (data.id == 0)
   					return false;

		        $.ajax({
		            url: self.baseUrl  + '/service/api/email/sendMail',
		            data: JSON.stringify(data),
		            contentType: 'application/json',
		            type: 'POST',
		            success: function (resp) {
		              //  var res = ('Thank you for sending your message to ' + resp.name + ' with email address ' + resp.email + ' and an id of ' + resp.id);
		               // $('#result').text(res);
		            },
		            error: function (resp) {
		                console.log('error');
		                console.log(resp);
		            },
		        });

   			},
   			shareViaPhoneApiCall: function(params) {
   				var self = this; 
				var data = {
					id: 0
				}; 

   				if (params) {
   					data.id = (params.id) ? (params.id) : 0;	
   				}

   				if (data.id == 0)
   					return false;

		    	$.ajax({
		            url: self.baseUrl + '/service/api/phone/' + data.id,
		            data: JSON.stringify(data),
		            contentType: 'application/json',
		            dataType: 'json',
		            type: 'POST',
		            success: function (resp) {
		                //var res = ('Hello, you made your call. ' + resp.result);
		                //$('#result').text(res);
		                self.data.callResult = resp.result;
		            },
		            error: function (resp) {
		                console.log('error');
		                console.log(resp);
		            },
		        });   				
   			},
   			saveEntry: function(params) {

   				var self = this; 

				var data = {
		            email: "",
		            name: "",
		            state: "NSW",
		            answer: "",
		            mumName: "",
		            songFileName: "",
		            terms: true
        		};

   				if (params) {
   					data.name = (params.name) ? (params.name) : '';	
   					data.email = (params.email) ? (params.email) : '';	
   					data.state = (params.state) ? (params.state) : 'NSW';	
   					data.answer = (params.answer) ? (params.answer) : '';	
   					data.songFileName = (params.songFileName) ? (params.songFileName) : self.data.mumName.songFileName;	
   					data.mumName = (params.mumName) ? (params.mumName) : self.data.mumName;	
   					data.terms = (params.terms) ? (params.terms) : true;	
   				}

		        $.ajax({
		            url: self.baseUrl + '/service/api/entry/saveEntry',
		            data: JSON.stringify(data),
		            contentType: 'application/json',
		            dataType: 'json',
		            type: 'POST',
		            success: function (resp) {
		                self.data.id = resp.id;
		            },
		            error: function (resp) {
		                console.log('error');
		                console.log(resp);
		            },
		        });   				

   			},
   			recordMix: function(params) {
   				var self = this; 
   				
				var data = {
		            melody: "A",
		            beats: "A",
		            effects: "A",
		            lyrics: ["A", "A", "A", "A"]
		        };

   				if (params) {
   					data.melody = (params.melody) ? (params.melody) : '';	
   					data.beats = (params.beats) ? (params.beats) : '';	
   					data.effects = (params.effects) ? (params.effects) : '';	
   					data.lyrics = (params.lyrics) ? (params.lyrics) : '';	
   				}

		        $.ajax({
		            url: self.baseUrl + '/service/api/mix/recordMix',
		            data: JSON.stringify(data),
		            contentType: 'application/json',
		            dataType: 'json',
		            type: 'POST',
		            success: function (resp) {
		            	self.data.songFileName = resp.path;

		            	var pathArray = resp.path.split('/'); 
		            	self.data.filename = pathArray[pathArray.length - 1];
		            },
		            error: function (resp) {
		                console.log('error');
		                console.log(resp);
		            },
		        });   				
   			},

   			setMumName: function (name) {
				this.data.mumName = name;
				//$rootScope.$broadcast('updatedData', self.data.mumName);

   			},
   			getMumName: function() {
   				return this.data.mumName;
   			},

   			/**
   			 * @since  preferred filename must be applied for the API
   			 * Instead of changing the filenames of each audio file,
   			 * convert its id here for corresponding filename
			 * @todo consider putting this on assets.js if possible
   			 *
   			 */
   			convertRecordMixKey: function(musicList) {
				var data = {
		            melody: "",
		            beats: "",
		            effects: "",
		            lyrics: ["", "", "", ""]
		        };

		        //melody
   				switch (musicList[0]) {
   					case 'icon-recorder':
   						data.melody = 'A';
   						break;
   					case 'icon-guitar':
   						data.melody = 'B';
   						break;
   					case 'icon-sax':
   						data.melody = 'C';
   						break;
   					case 'icon-trumpet':
   						data.melody = 'D';
   						break;
   					case 'icon-guitar-acoustic':
   						data.melody = 'E';
   						break;
   				}

		        //beats
   				switch (musicList[1]) {
   					case 'icon-bong':
   						data.beats = 'A';
   						break;
   					case 'icon-shaker':
   						data.beats = 'B';
   						break;
   					case 'icon-drum-head':
   						data.beats = 'C';
   						break;
   					case 'icon-snare':
   						data.beats = 'D';
   						break;
   					case 'icon-percussion':
   						data.beats = 'E';
   						break;
   				}

		        //effects
   				switch (musicList[2]) {
   					case 'icon-cymbals-blue':
   						data.effects = 'A';
   						break;
   					case 'icon-bell':
   						data.effects = 'B';
   						break;
   					case 'icon-maraca':
   						data.effects = 'C';
   						break;
   					case 'icon-cymbals':
   						data.effects = 'D';
   						break;
   					case 'icon-triangle':
   						data.effects = 'E';
   						break;
   				}

   				var alpha = ['A','B','C','D', 'E','F','G','H','I','J','K','L','M','N','O'];
		        
		        //Sweet
   				var sweetIndex  = parseInt(musicList[3].substring(2)) - 1;
   				data.lyrics[0] = alpha[sweetIndex];

   				//Nice
   				var niceIndex  = parseInt(musicList[4].substring(2)) - 1;
   				data.lyrics[1] = alpha[niceIndex];

   				//Nuaghty
   				var naughtyIndex  = parseInt(musicList[5].substring(2)) - 1;
   				data.lyrics[2] = alpha[naughtyIndex];

   				//Forgiving
   				var forgivingIndex  = parseInt(musicList[6].substring(2)) - 1;
   				data.lyrics[3] = alpha[forgivingIndex];


   				return data;
   			},

   			getFileName: function() {

   			}

   		}
 }]);