'use strict';

angular.module('musicmixingApp')
	.controller('ThankYouController',['$scope','Audioplayer','AudioMixer','$location','HttpService',
			function ($scope, Audioplayer, AudioMixer, $location, HttpService) {
			
			$scope.currentPage = window.location.host; 
			$scope.isPhoneCalled = false;
			$scope.isEmailed = false;

			$scope.phoneTab = function() {
				$('.tabs li:last').removeClass('active');
				$('.tabs li:first').addClass('active');
				$('#email').css('display','none');
				$('#phone').fadeIn();
			};

			$scope.emailTab = function() {
				$('.tabs li:first').removeClass('active');
				$('.tabs li:last').addClass('active');
				$('#phone').css('display','none');
				$('#email').fadeIn();
			};

			$scope.phoneCall = function() {
				$('.api-error').css('display','none');
				
				if ($scope.isPhoneCalled)
					return;

				if (HttpService.data.id == 0) {
					$('.api-error').fadeIn();
					return;
				}
				$('.api-error').fadeOut();

				HttpService.shareViaPhoneApiCall({id:HttpService.data.id});
				$scope.isPhoneCalled = true;

				$('#phone .phone-title').css('display','none');
				$('#phone .grid-4 .form-category').css('display','none');
				$('#phone .btn-call').css('display','none');

			};

			$scope.sendEmail = function() {
				$('.api-error').css('display','none');

				if ($scope.isEmailed)
					return;

				if (HttpService.data.id == 0) {
					$('.api-error').fadeIn();
					return;
				}
				$('.api-error').fadeOut();

				var params = {
					id: HttpService.data.id,
					name: HttpService.data.name,
					email: HttpService.data.email
				};

				HttpService.sendEmail(params);
				$scope.isEmailed = true;

				$('.email-title').css('display','none');
				$('.email-form .form-category').css('display','none');
				$('.btn-send').css('display','none');

			};

			$scope.fbShare = function() {
				 $location.path('/thank-you');
			};	
		}]
	);	