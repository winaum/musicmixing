'use strict';

angular.module('musicmixingApp')
	.controller('NameController',['$scope','Audioplayer','AudioMixer','$location','HttpService',
			function ($scope, Audioplayer, AudioMixer, $location,HttpService) {
			
			$scope.errorMessage = '';
			$scope.continue = function() {

				var title = $('#mom-title').val(),
					name  = $('#mom-name').val();

				if (title == '' || name == '') {
					$scope.errorMessage = 'Required field is missing.';
					return false;
				}

				HttpService.data.mumName = name;
				HttpService.data.title = title;
				
				$location.path('/submit-record');
			};	
		}]
	);