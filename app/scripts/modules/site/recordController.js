'use strict';

angular.module('musicmixingApp')
	.controller('RecordController',['$scope','Audioplayer','AudioMixer','$location','HttpService',
			function ($scope, Audioplayer, AudioMixer, $location, HttpService) {
			
			$scope.errorMessage = '';
			
			$scope.submit = function() {
				
				/**
				 *@todo use angular validation
				 */
				 var name = $('#name').val(),
				 	 email = $('#email').val(),
				 	 state = $('#state').val(),
				 	 answer = $('#answer').val(),
				 	 agreeToTerms = $('.mm-terms').prop('checked');

				 if (name == '' || email == '' || state == '' || answer == '') {
				 	$scope.errorMessage = 'Required field is missing.';
				 	return false;
				 } 	

				 if (!agreeToTerms) {
				 	$scope.errorMessage = 'You must agree with the terms and conditions.';
				 	return false;
				 }


				 if (/@/.test(email) == false) {
				 	$scope.errorMessage = 'Invalid email address.';
				 	return false;
				 }

				 HttpService.data.name = name;
				 HttpService.data.email = email;
				 HttpService.data.state = state;
				 HttpService.data.answer = answer;
				 HttpService.data.terms = (agreeToTerms) ? true : false;
				 
				 var params = {
		            email: email,
		            name: name,
		            state: state,
		            answer: answer,
		            mumName: HttpService.data.mumName,
		            songFileName: HttpService.data.filename,
		            terms: true
        		 };

				 HttpService.saveEntry(params);
				 $location.path('/thank-you');
			};	
		}]
	);	