'use strict';

angular.module('musicmixingApp')
	.controller('BeatsController', ['$scope','Audioplayer','AudioMixer',	
			function ($scope, Audioplayer, AudioMixer) {
				$scope.circleOption = ['icon-bong','icon-drum-head','icon-percussion','icon-snare','icon-shaker'];
				$scope.selectedBeats = '' + AudioMixer.musics.getById(1);

				var $container = $('.record-player');

				var $blueRing  = $container.find('.beats-ring-blue');

				if (($scope.selectedBeats + '') != 'undefined') {
					$blueRing.fadeIn();
				} 

				if (AudioMixer.musics.getById(0) + ''  != 'undefined') {
					$('.needle').addClass('pulse-effect');
				}

				AudioMixer.musics.showInstrumentsAnimation($container);					

				$scope.setBeats = function (beats) {
					$('ul.icon-nav').find('li').removeClass('active');
					var $parent = $('.' + beats).parent('div').parent('li');
					$parent.addClass('active');
					$('.needle').addClass('pulse-effect');

					AudioMixer.musics.addBeats(beats);
					AudioMixer.musics.playTimeLine();	
					$blueRing.fadeIn();
					AudioMixer.musics.showInstrumentsAnimation($container);					

				};
		}]
	);