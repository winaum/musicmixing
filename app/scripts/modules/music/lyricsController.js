'use strict';

angular.module('musicmixingApp')
	.controller('LyricsController', ['$scope','Audioplayer','AudioMixer','$location','HttpService',
		function ($scope,Audioplayer,AudioMixer,$location, HttpService) {

			var radius = 258;

			if(screen.width <= 640) {
		 		radius = 190;
		 		$('.left').css('left','-56%');
		 		$('.left').css('top','46%');
		 		$('.right').css('right','-55%');
		 		$('.right').css('top','46%');
		 		$('.top').css('left','0px');
		 		$('.inner .rotation-content .bottom').css('top','102%');
		 		$('.outer .rotation-content .bottom').css('bottom','-22px');
		 		$('.rotation-container').css('font-size','11px !important');
			}

			$('.main-content').addClass('lyrics');
			$('.header').addClass('hide-on-light');

			$scope.lyricsTypes = ['Sweet','Nice','Naughty','Forgiving'];
			// $('.lyrics-type').circleType({radius: radius, dir: -1});

			//default to Sweet
			$scope.activeType = 'Sweet';
			//AudioMixer.musics.generateDefaultLyrics();
			$scope.lyricPosition = {'Sweet': 0,'Nice':0,'Naughty':0,'Forgiving':0};;

			$('.lyric').text('' + AudioMixer.musics.getLyric($scope.activeType));
			$('.outer .bend').circleType({radius: radius, dir: -1});

			//hide record button if track is not complete
			$scope.isTrackComplete = (AudioMixer.musics.countTrackList() == 7) ? true : false;


			$scope.prevType = function (el) {
				$('.rotate-prev').removeClass('active');
				$('.rotate-next').addClass('active');

				var indexType = 0;
				var lastType = $scope.lyricsTypes.pop($scope.lyricsTypes[$scope.lyricsTypes.length - 1]);


				$scope.lyricsTypes.unshift(lastType);

				$('.lyrics-type').each(function(index, el){
					$(this).text('');
					$(this).text(' ' + $scope.lyricsTypes[indexType]);
					indexType++;
				});


				//get lyrics from Lyrics object
				$scope.activeType = $scope.lyricsTypes[0];
				$scope.playLyric();

				$('.lyric').text('' + AudioMixer.musics.getLyric($scope.activeType));
				$('.outer .bend').circleType({radius: radius, dir: -1});
				// $('.lyrics-type').circleType({radius: radius, dir: -1});
		 		$('.inner .rotation-content .bottom').css('bottom','-22px');
		 		console.log(AudioMixer.musics.items);
		 		$scope.isTrackComplete = (AudioMixer.musics.countTrackList() == 7) ? true : false;

			};

			$scope.nextType = function () {
				$('.rotate-next').removeClass('active');
				$('.rotate-prev').addClass('active');

				var indexType = 0;
				var firstType = $scope.lyricsTypes.shift($scope.lyricsTypes[$scope.lyricsTypes.length - 1]);

				$scope.lyricsTypes.push(firstType);

				$('.lyrics-type').each(function(index, el){
					$(this).text('');
					$(this).text(' ' + $scope.lyricsTypes[indexType]);
					indexType++;
				});

				$scope.activeType = $scope.lyricsTypes[0];

				$('.lyric').text('' + AudioMixer.musics.getLyric($scope.activeType));

				$('.outer .bend').circleType({radius: radius, dir: -1});
				// $('.lyrics-type').circleType({radius: radius, dir: -1});
		 		// $('.inner .rotation-content .bottom').css('bottom','-22px');
				$scope.playLyric();

		 		$scope.isTrackComplete = (AudioMixer.musics.countTrackList() == 7) ? true : false;
			};

			$scope.moveDown = function() {
				$scope.lyricPosition[$scope.activeType]++;

				if ($scope.lyricPosition[$scope.activeType] >= Lyrics[$scope.activeType].length) {
					$scope.lyricPosition[$scope.activeType] = (Lyrics[$scope.activeType].length - 1);

					return false;
				}

				$('.rotate-up').removeClass('active');
				$('.rotate-down').addClass('active');
				$('.lyric').text('' + Lyrics[$scope.activeType][$scope.lyricPosition[$scope.activeType]]);
				$('.outer .bend').circleType({radius: radius, dir: -1});
				$scope.playLyric();

			};

			$scope.moveUp = function() {
				$scope.lyricPosition[$scope.activeType]--;

				if ($scope.lyricPosition[$scope.activeType] < 0) {
					$scope.lyricPosition[$scope.activeType]  = 0;
					return false;
				}

				$('.rotate-down').removeClass('active');
				$('.rotate-up').addClass('active');
				$('.lyric').text('' + Lyrics[$scope.activeType][$scope.lyricPosition[$scope.activeType]]);
				$('.outer .bend').circleType({radius: radius, dir: -1});
				$scope.playLyric();

			};

			$scope.playLyric = function() {

				$('.lyrics-loader').fadeIn();

				var audioFile = '1_';

				switch ($scope.activeType) {
					case 'Sweet': {
						audioFile = '1_' + ($scope.lyricPosition[$scope.activeType] + 1);
						AudioMixer.musics.addSweet(audioFile);
						break;
					}
					case 'Nice': {
						audioFile = '2_' + ($scope.lyricPosition[$scope.activeType] + 1);
						AudioMixer.musics.addNice(audioFile);
						break;
					}
					case 'Naughty': {
						audioFile = '3_' + ($scope.lyricPosition[$scope.activeType] + 1);
						AudioMixer.musics.addNaughty(audioFile);
						break;
					}
					case 'Forgiving': {
						audioFile = '4_' + ($scope.lyricPosition[$scope.activeType] + 1);
						AudioMixer.musics.addForgiving(audioFile);
						break;
					}
				}

				AudioMixer.musics.playTimeLine();

			};

			$scope.record = function() {

				AudioMixer.musics.haltTrack();
				var recordParams = HttpService.convertRecordMixKey(AudioMixer.musics.items);
				HttpService.recordMix(recordParams);
				$location.path('/enter-name');
			};

			$scope.muteTrack = function () {
				$('.lyrics-loader').fadeOut();
				AudioMixer.musics.haltTrack();
			};

		}]
	);