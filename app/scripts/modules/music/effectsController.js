'use strict';

angular.module('musicmixingApp')
	.controller('EffectsController',['$scope','Audioplayer','AudioMixer',	
		function ($scope, Audioplayer, AudioMixer) {
			$scope.circleOption = ['icon-cymbals','icon-maraca','icon-triangle','icon-bell','icon-cymbals-blue'];			
			$scope.selectedEffects = '' + AudioMixer.musics.getById(2);

			var $container = $('.record-player');

			var	$blueRing  = $container.find('.effect-ring-blue');

			if (($scope.selectedEffects + '') != 'undefined') {
				$blueRing.fadeIn();
				$('.needle').addClass('pulse-effect');
			}   
			
			AudioMixer.musics.showInstrumentsAnimation($container);

			$scope.setEffects = function (effects) {
				$('ul.icon-nav').find('li').removeClass('active');
				var $parent = $('.' + effects).parent('div').parent('li');
				$parent.addClass('active');
				$('.needle').addClass('pulse-effect');

				AudioMixer.musics.addEffects(effects);
				AudioMixer.musics.playTimeLine();	
				
				$blueRing.fadeIn();
				AudioMixer.musics.showInstrumentsAnimation($container);

			};			
		}]
	);