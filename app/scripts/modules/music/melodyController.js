'use strict';

angular.module('musicmixingApp')
	.controller('MelodyController',['$scope','Audioplayer','AudioMixer',
			function ($scope, Audioplayer, AudioMixer) {
				$scope.circleOption = ['icon-guitar','icon-guitar-acoustic','icon-sax','icon-trumpet','icon-recorder'];
				$scope.selectedMelody = '' + AudioMixer.musics.getById(0);
				var $container = $('.record-player');
				var	$blueRing  = $container.find('.melody-ring-blue');


				$scope.setMelody = function (melody) {
					$('ul.icon-nav').find('li').removeClass('active');
					var $parent = $('.' + melody).parent('div').parent('li');
					$parent.addClass('active');
					$('.needle').addClass('pulse-effect');
					//AudioMixer.musics.haltTrack();

					AudioMixer.musics.addMelody(melody);
					AudioMixer.musics.playTimeLine();
					$blueRing.fadeIn();

					AudioMixer.musics.showInstrumentsAnimation($container);
				};

				if (($scope.selectedMelody + '') != 'undefined') {
					AudioMixer.musics.showInstrumentsAnimation($container);
					$blueRing.fadeIn();
					$('.needle').addClass('pulse-effect');
				}   

		}]
	);