'use strict';

angular.module('musicmixingApp')
  .controller('MainController', ['$scope','$routeParams', 'HttpService','Audioplayer',
  	function ($scope,$routeParams, HttpService) {
  		var id = ($routeParams.id) ? $routeParams.id : 0;

      HttpService.getRandomMix(id);   
      $scope.$on('updateSong', function(e, params){

          var waiting = setInterval(function() {
              if (soundManagerReady) {
                soundManager.createSound({
                  id: 'random-music',
                  url: HttpService.baseUrl + HttpService.data.songFileName,
                  autoLoad: true,
                  autoPlay: true,
                  onfinish: function(){             
                    this.unload();
                  }
                }); 
                clearInterval(waiting);

              }
          },1000); 

      });

      $scope.play  = function () {
        soundManager.stopAll();
        soundManager.play('random-music');
        /*  
        var waiting = setInterval(function() {  
              if (soundManagerReady) {
                soundManager.createSound({
                  id: 'random-music',
                  url: HttpService.baseUrl + HttpService.data.songFileName,
                  autoLoad: true,
                  autoPlay: true,
                  onfinish: function(){             
                    this.unload();
                  }
                }); 
                clearInterval(waiting);

              }
        },1000);       

        */  
      };

  }]);
