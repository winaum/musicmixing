'use strict';

var soundManagerReady = false;

soundManager.setup({
	url: 'bower_components/soundmanager2/swf/',
	flashVersion: 9, // optional: shiny features (default = 8)
	// optional: ignore Flash where possible, use 100% HTML5 mode
	preferFlash: false,
	useHighPerformance: true,	 
	onready: function() {
		soundManagerReady = true;
		// Ready to use; soundManager.createSound() etc. can now be called.
	},
	ontimeout: function() {
		/*console.log('Sound manager failed to load');*/
	}
});

angular
  .module('musicmixingApp', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
  ])
  .run(['$rootScope', '$location',function($rootScope, $location){
	  	/*
	     * workaround for browser back button
	     * @see http://docs.angularjs.org/api/ngRoute.$route
	     *		
		 */  		
		//store current url
    	$rootScope.$on('$locationChangeSuccess', function(){
        	$rootScope.actualLocation = $location.path();

    	});

   		$rootScope.$watch(
   			function() {
   				return $location.path()
   			}, 
   			function(newLocation, oldLocation){

	        	if($rootScope.actualLocation == newLocation){
	            	window.location.reload();
        		}
   		 });
		
    }]);
