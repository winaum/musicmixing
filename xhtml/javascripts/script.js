/* Author:

*/


// Global Variables

var isMobile = /Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent);

(function($) {
	function initMobile() {
		$('body').prepend('<div class="mobile-menu-bg"></div>');
		var navbar = $('.navbar');
		var navToggle =  $('.nav-switch');
		var navCollapse = $('.nav-fold');
		var $navList = $('.nav').find('li');
		navToggle.on('click', function(event) {
			if (navCollapse.height() === 0) {
				var curHeight = navCollapse.height();
				var autoHeight = navCollapse.css('height', 'auto').height();
				navCollapse.height(curHeight).animate({
					height : autoHeight									 
				}, 200, function() {
					navCollapse.css('height', 'auto');
					
				});	
				$('.mobile-menu-bg').fadeIn();
			} else {
				navCollapse.height(curHeight).animate({
					height : 0								 
				}, 200);
				$('.mobile-menu-bg').hide();
			}
			event.stopPropagation();
		});
		if (isMobile) {
			$navList.on('click', function() {
				navCollapse.height(navCollapse.height()).animate({
					height : 0								 
				}, 200);
				$('.mobile-menu-bg').hide();
			});
			navToggle.on('click', function(event) {
				if (navCollapse.height() === 0) {
					var curHeight = navCollapse.height();
					var autoHeight = navCollapse.css('height', 'auto').height();
					navCollapse.height(curHeight).animate({
						height : autoHeight									 
					}, 200, function() {
						navCollapse.css('height', 'auto');
					});	
					$('.mobile-menu-bg').fadeIn();
				} else {
					navCollapse.height(curHeight).animate({
						height : 0								 
					}, 200);
					navbar.removeClass('open');
					$('.mobile-menu-bg').hide();
				}
				event.stopPropagation();
			});
			$(document).on('click', function() {
				navCollapse.height(navCollapse.height()).animate({
					height : 0								 
				}, 200);
				$('.mobile-menu-bg').hide();
			});
		}
	}


	function initDropdown() {
		$('.dropdown').each(function() {
			var $this = $(this);
			var $dropdown = $this.find('.dropdown-content');
			$this.on('mouseenter', function() {
				$dropdown.fadeIn('fast');
				$this.addClass('active');
			}).on('mouseleave', function() {
				$dropdown.fadeOut('fast');
				$this.removeClass('active');
			});
			if (isMobile) {
				$this.unbind('mouseenter mouseleave');
				$this.on('click', function() {
					$this.toggleClass('dropdown-open');
					$this.toggleClass('active');
				});
			}
		});
	}

	
	function initTab() {
		var $tab = $('.tab');
		$.each($tab, function() {
			var $this = $(this);
			$currentIndex = 0;

			var $tabList = $this.find('.tabs li');
			var $tabContent = $this.find('.tab-content');
			$tabContent.addClass('inactive');
			$tabContent.eq($currentIndex).removeClass('inactive').addClass('active');
			$tabList.eq($currentIndex).addClass('active');
			//console.log($this);

			$tabList.on('click', function() {
				var $thisIndex = $(this).index();
				$tabList.removeClass('active');
				$(this).addClass('active');
				$tabContent.removeClass('active').addClass('inactive');
				$tabContent.eq($thisIndex).addClass('active').removeClass('inactive');
				//console.log($thisIndex);
				return false;
			});
		});
	}

	$(document).ready(function() {
		$('input, textarea').placeholder();
		initMobile();
		initDropdown();
		initTab();
		$('.icheck').iCheck({
	 		checkboxClass: 'icheckbox',
	        radioClass: 'iradio'
		});
		$(function() {
		    FastClick.attach(document.body);
		});
		$('.label-top .bend').circleType({radius: 65});
		$('.outer .bend').circleType({radius: 258, dir: -1});
		if (isMobile || $(window).width() < 767) {
			$('.outer .bend').circleType({radius: 175, dir: -1});
			$('.label-top .bend').circleType({radius: 45});
		}
	});
	
})(jQuery);