'use strict';
angular.module('musicmixingApp', [
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'ngRoute'
]);
'use strict';
angular.module('musicmixingApp').config([
  '$routeProvider',
  function ($routeProvider) {
    $routeProvider.when('/', {
      templateUrl: 'scripts/modules/home/views/main.html',
      controller: 'MainCtrl'
    }).when('/song-creator/', { redirectTo: '/song-creator/melody-select' }).when('/song-creator/melody-select', {
      templateUrl: 'scripts/modules/song/views/melody-select.html',
      controller: 'MelodySelectCtrl'
    }).when('/song-creator/beat-select', {
      templateUrl: 'scripts/modules/song/views/beat-select.html',
      controller: 'BeatSelectCtrl'
    }).when('/song-creator/effects-select', {
      templateUrl: 'scripts/modules/song/views/effects-select.html',
      controller: 'EffectsSelectCtrl'
    }).when('/song-creator/lyrics-select', {
      templateUrl: 'scripts/modules/song/views/lyrics-select.html',
      controller: 'LyricsSelectCtrl'
    }).otherwise({ redirectTo: '/' });
  }
]);
'use strict';
angular.module('musicmixingApp').controller('FooterCtrl', [
  '$scope',
  '$location',
  function ($scope, $location) {
    $scope.isHomePage = $location.path() === '/';
    $scope.$on('$routeChangeSuccess', function () {
      $scope.isHomePage = $location.path() === '/';
      $scope.step = $scope.getStepNumber($location.path());
    });
    $scope.getStepNumber = function (currentLocation) {
      switch (currentLocation) {
      case '/song-creator/melody-select':
        return 1;
        break;
      case '/song-creator/beat-select':
        return 2;
        break;
      case '/song-creator/effects-select':
        return 3;
        break;
      case '/song-creator/lyrics-select':
        return 4;
        break;
      }
    };
    $scope.step = $scope.getStepNumber($location.path());
  }
]);
'use strict';
angular.module('musicmixingApp').controller('MainCtrl', [
  '$scope',
  function ($scope) {
    $scope.awesomeThings = [
      'HTML5 Boilerplate',
      'AngularJS',
      'Karma'
    ];
  }
]);
'use strict';
angular.module('musicmixingApp').controller('BeatSelectCtrl', [
  '$scope',
  function ($scope) {
  }
]);
'use strict';
angular.module('musicmixingApp').controller('EffectsSelectCtrl', [
  '$scope',
  function ($scope) {
  }
]);
'use strict';
angular.module('musicmixingApp').controller('LyricsSelectCtrl', [
  '$scope',
  function ($scope) {
  }
]);
'use strict';
angular.module('musicmixingApp').controller('MelodySelectCtrl', [
  '$scope',
  function ($scope) {
  }
]);